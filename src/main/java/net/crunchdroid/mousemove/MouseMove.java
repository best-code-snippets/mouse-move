package net.crunchdroid.mousemove;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

public class MouseMove extends JPanel implements ActionListener {

    public static int DELAY_IN_SECONDS = 60 * 1000;
    public static final int MAX_Y = 400;
    public static final int MAX_X = 400;
    private final JButton start;
    private final JButton stop;
    private final JSlider slider;
    private final JLabel label;
    private final MouseThread mouse;

    public MouseMove() {
        mouse = new MouseThread();

        start = new JButton("Start");
        start.setMnemonic(KeyEvent.VK_E);
        start.setActionCommand("start");
        start.addActionListener(this);
        start.setEnabled(true);

        stop = new JButton("Stop");
        stop.setMnemonic(KeyEvent.VK_E);
        stop.setActionCommand("stop");
        stop.addActionListener(this);
        stop.setEnabled(false);

        label = new JLabel("Every 1 minute");

        slider = new JSlider(1, 10, 1);
        slider.setMinorTickSpacing(1);
        slider.setMajorTickSpacing(1);
        slider.setPaintTicks(true);
        slider.setSnapToTicks(true);
        slider.addChangeListener(e -> {
            int value = slider.getValue();
            DELAY_IN_SECONDS = value * 60 * 1000;
            label.setText(String.format("Every %d %s", value, value > 1 ? "minutes" : "minute"));
            System.out.println("Value: " + DELAY_IN_SECONDS);
        });


        add(start);
        add(stop);
        add(slider);
        add(label);
    }

    private static void createWindow() {

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        MouseMove content = new MouseMove();
        content.setOpaque(true);

        JFrame frame = new JFrame("Mouse Move");
        frame.setPreferredSize(new Dimension(300, 150));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
        frame.setContentPane(content);
        frame.pack();
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();

        if ("start".equals(action)) {
            start.setEnabled(false);
            slider.setEnabled(false);
            stop.setEnabled(true);
            mouse.start(DELAY_IN_SECONDS);
        } else if ("stop".equals(action)) {
            start.setEnabled(true);
            slider.setEnabled(true);
            stop.setEnabled(false);
            mouse.stop();
        }
    }

    public static void main(String... args) {
        createWindow();
    }

    public static class MouseThread implements Runnable {

        private int interval;
        private Thread worker;
        private final AtomicBoolean running = new AtomicBoolean(false);
        private final AtomicBoolean stopped = new AtomicBoolean(false);

        public MouseThread() {
        }

        public void start(int interval) {
            this.interval = interval;
            worker = new Thread(this);
            worker.start();
        }

        public void stop() {
            running.set(false);
            worker.interrupt();
        }

        boolean isRunning() {
            return running.get();
        }

        boolean isStopped() {
            return stopped.get();
        }

        @Override
        public void run() {
            running.set(true);
            stopped.set(false);
            while (running.get()) {
                try {
                    Thread.sleep(interval);
                    Random random = new Random();
                    Robot robot = new Robot();
                    robot.mouseMove(random.nextInt(MAX_X), random.nextInt(MAX_Y));
                    robot.setAutoDelay(DELAY_IN_SECONDS);
                } catch (InterruptedException | AWTException e) {
                    Thread.currentThread().interrupt();
                    System.out.println("Thread was interrupted, Failed to complete operation");
                }
            }
            stopped.set(true);
        }
    }

}
